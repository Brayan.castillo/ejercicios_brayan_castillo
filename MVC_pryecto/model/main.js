var MVC = {};

MVC.controllerInstance = new MVC.controllerInstance({
    model: MVC.Model,
    view: MVC.View,
    contentElement: document.querySelector('#contact_form'),
    endpoint: './model/persona.json'
});

document.body.dispatchEvent(new Event('onloadApp') );
