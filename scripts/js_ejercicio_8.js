var personArr = [
    {
        "personId": 123,
        "name": "John",
        "city": "Melbourne",
        "phoneNo": "1234567890"
    },
    {
        "personId": 124,
        "name": "Amelia",
        "city": "Sydney",
        "phoneNo": "1234567890"
    },
    {
        "personId": 125,
        "name": "Emily",
        "city": "Perth",
        "phoneNo": "1234567890"
    },
    {
        "personId": 126,
        "name": "Abraham",
        "city": "Perth",
        "phoneNo": "1234567890"
    }
];

function genera_tabla() {
    // Obtener la referencia del elemento body
    var body = document.getElementsByTagName("body")[0];
   
    // Crea un elemento <table> y un elemento <tbody>
    var tabla   = document.createElement("table");
    var tblBody = document.createElement("tbody");
   
    // Crea las celdas
    for (var i = 0; i < personArr.length; i++) {
      // Crea las hileras de la tabla
      var hilera = document.createElement("tr");
   
      for (var j = 0; j < 1; j++) {
        // Crea un elemento <td> y un nodo de texto, haz que el nodo de
        // texto sea el contenido de <td>, ubica el elemento <td> al final
        // de la hilera de la tabla
        var celda1 = document.createElement("td");
        var textoCelda1 = document.createTextNode('id: ' + personArr[i].personId);
        celda1.appendChild(textoCelda1);
        hilera.appendChild(celda1); 

        var celda2 = document.createElement("td");
        var textoCelda2 = document.createTextNode('Nombre: ' + personArr[i].name);
        celda2.appendChild(textoCelda2);
        hilera.appendChild(celda2); 

        var celda3= document.createElement("td");
        var textoCelda3 = document.createTextNode('Ciudad: ' + personArr[i].city);
        celda3.appendChild(textoCelda3);
        hilera.appendChild(celda3); 

        var celda4 = document.createElement("td");
        var textoCelda4 = document.createTextNode('Numero Telefonico: ' + personArr[i].phoneNo);
        celda4.appendChild(textoCelda4);
        hilera.appendChild(celda4); 
      }
   
      // agrega la hilera al final de la tabla (al final del elemento tblbody)
      tblBody.appendChild(hilera);
    }
   
    // posiciona el <tbody> debajo del elemento <table>
    tabla.appendChild(tblBody);
    // appends <table> into <body>
    body.appendChild(tabla);
    // modifica el atributo "border" de la tabla y lo fija a "2";
    tabla.setAttribute("border", "2");
  }